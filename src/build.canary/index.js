const electron = require('electron')
const app = electron.app
const fs = require('fs')
const path = require('path')
const url = require('url')
const {ipcMain} = require('electron')

if (handleSquirrelEvent()) {
    return
}

function handleSquirrelEvent() {
    if (process.argv.length === 1) {
        return false;
    }

    const ChildProcess = require('child_process')

    const appFolder = path.resolve(process.execPath, '..')
    const rootAtomFolder = path.resolve(appFolder, '..')
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'))
    const exeName = path.basename(process.execPath)

    const spawn = function(command, args) {
        let spawnedProcess, error

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {detached: true})
        } catch (error) {}

        return spawnedProcess
    }

    const spawnUpdate = function(args) {
        return spawn(updateDotExe, args)
    }

    const squirrelEvent = process.argv[1]
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            spawnUpdate(['--createShortcut', exeName])
			
			app.setAsDefaultProtocolClient('sabat-czarownic')
			
            setTimeout(app.quit, 1000)
            return true

        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers

            // Remove desktop and start menu shortcuts
            spawnUpdate(['--removeShortcut', exeName])
			
			app.removeAsDefaultProtocolClient('sabat-czarownic')

            setTimeout(app.quit, 1000)
            return true

        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated

            app.quit()
            return true
    }
}

const BrowserWindow = electron.BrowserWindow

let mainWindow

function createWindow () {
    mainWindow = new BrowserWindow({
        width: 1280,
        height: 768,
        icon: path.join(__dirname, 'img/canaryicon.png'),
        frame: false,
        title: 'Spy',
        backgroundColor: '#242424'
    })

    if (fs.existsSync(`${__dirname}/../webpack.canary.config.js`)) {
        mainWindow.loadURL('http://localhost:8080/bundle')
    } else {
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, 'index.html'),
            protocol: 'file:',
            slashes: true
        }))
    }

    mainWindow.on('closed', () => {
        mainWindow = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow()
    }
})

ipcMain.on('notification-request', (event, html) => {
    let notificationwindow

    notificationwindow = new BrowserWindow({
        width: 350,
        height: 92,
        frame: false,
        backgroundColor: '#fff',
        parent: mainWindow
    })

    notificationwindow.setAlwaysOnTop(true)

    notificationwindow.loadURL(url.format({
        pathname: 'text/html;' + html,
        protocol: 'data:',
        slashes: true
    }))

    notificationwindow.on('closed', () => {
        notificationwindow = null
    })
})