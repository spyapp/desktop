/*
 * SpyApp
 * Copyright (c) 2017 rylat
*/
global.isCanary = true;
console.log("%cHOLD UP!", "color: blue; font-size: 90px; font-weight: bold;");
console.log("%cIf someone asked you to enter here some code, there's 11/10 chance you're being scammed!", "color: red; font-size:15px;");
require('./storage.js');
require('./saveWindowBounds.js');
require('./sass/normalize.scss');
require('./fonts/sourcesanspro.scss');
require('./sass/movable.scss');
require('./sass/border.scss');
require('./sass/tabs.scss');
require('./sass/context-menu.scss');
require('./sass/flex.scss');
require('./sass/dialog.scss');
require('./sass/profile.scss');
require('./sass/main.scss');
require('./render.js');
require('./websocketsNotice.js');
require('./loadPersistingStreamers.js');