const { remote } = require('electron')
document.getElementsByClassName('window_decoration__controls__close')[0].addEventListener('click', ()=>{
    let window = remote.getCurrentWindow()
    window.close()
})
document.getElementsByClassName('window_decoration__controls__minimize')[0].addEventListener('click', ()=>{
    let window = remote.getCurrentWindow()
    window.minimize()
})
document.getElementsByClassName('window_decoration__controls__maximize')[0].addEventListener('click', ()=>{
    let window = remote.getCurrentWindow()
    window.maximize()
})