const Taiga = require('./taiga')
const renderTab = require('./tabRender.js')

let gearspingif = 'data:image/svg+xml;base64,PHN2ZyBjbGFzcz0ibGRzLWdlYXIiIHdpZHRoPSIyMDBweCIgIGhlaWdodD0iMjAwcHgiICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiIHN0eWxlPSJiYWNrZ3JvdW5kOiBub25lOyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTAgNTApIj48ZyB0cmFuc2Zvcm09InJvdGF0ZSgxNjIpIj48YW5pbWF0ZVRyYW5zZm9ybSBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iIHR5cGU9InJvdGF0ZSIgdmFsdWVzPSIwOzM2MCIga2V5VGltZXM9IjA7MSIgZHVyPSIxcyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiPjwvYW5pbWF0ZVRyYW5zZm9ybT48cGF0aCBkPSJNMzcuNDM5OTUxOTIzMDQ2MDUgLTYuNSBMNDcuNDM5OTUxOTIzMDQ2MDUgLTYuNSBMNDcuNDM5OTUxOTIzMDQ2MDUgNi41IEwzNy40Mzk5NTE5MjMwNDYwNSA2LjUgQTM4IDM4IDAgMCAxIDM1LjY3Mzk0OTQ4MTgyNTkzIDEzLjA5MDgxMDgzNjkyNDE3NCBMMzUuNjczOTQ5NDgxODI1OTMgMTMuMDkwODEwODM2OTI0MTc0IEw0NC4zMzQyMDM1MTk2NzAzMiAxOC4wOTA4MTA4MzY5MjQxNzQgTDM3LjgzNDIwMzUxOTY3MDMyIDI5LjM0OTE0MTA4NjEyMTg4IEwyOS4xNzM5NDk0ODE4MjU5MyAyNC4zNDkxNDEwODYxMjE4OCBBMzggMzggMCAwIDEgMjQuMzQ5MTQxMDg2MTIxODggMjkuMTczOTQ5NDgxODI1OTMgTDI0LjM0OTE0MTA4NjEyMTg4IDI5LjE3Mzk0OTQ4MTgyNTkzIEwyOS4zNDkxNDEwODYxMjE4OCAzNy44MzQyMDM1MTk2NzAzMiBMMTguMDkwODEwODM2OTI0MTg0IDQ0LjMzNDIwMzUxOTY3MDMyIEwxMy4wOTA4MTA4MzY5MjQxODMgMzUuNjczOTQ5NDgxODI1OTMgQTM4IDM4IDAgMCAxIDYuNSAzNy40Mzk5NTE5MjMwNDYwNSBMNi41IDM3LjQzOTk1MTkyMzA0NjA1IEw2LjUwMDAwMDAwMDAwMDAwMSA0Ny40Mzk5NTE5MjMwNDYwNSBMLTYuNDk5OTk5OTk5OTk5OTk1IDQ3LjQzOTk1MTkyMzA0NjA2IEwtNi40OTk5OTk5OTk5OTk5OTYgMzcuNDM5OTUxOTIzMDQ2MDYgQTM4IDM4IDAgMCAxIC0xMy4wOTA4MTA4MzY5MjQxNyAzNS42NzM5NDk0ODE4MjU5MyBMLTEzLjA5MDgxMDgzNjkyNDE3IDM1LjY3Mzk0OTQ4MTgyNTkzIEwtMTguMDkwODEwODM2OTI0MTcgNDQuMzM0MjAzNTE5NjcwMzIgTC0yOS4zNDkxNDEwODYxMjE4NyAzNy44MzQyMDM1MTk2NzAzMjUgTC0yNC4zNDkxNDEwODYxMjE4NzIgMjkuMTczOTQ5NDgxODI1OTM2IEEzOCAzOCAwIDAgMSAtMjkuMTczOTQ5NDgxODI1OTIgMjQuMzQ5MTQxMDg2MTIxODkgTC0yOS4xNzM5NDk0ODE4MjU5MiAyNC4zNDkxNDEwODYxMjE4OSBMLTM3LjgzNDIwMzUxOTY3MDMxIDI5LjM0OTE0MTA4NjEyMTg5MyBMLTQ0LjMzNDIwMzUxOTY3MDMxIDE4LjA5MDgxMDgzNjkyNDIgTC0zNS42NzM5NDk0ODE4MjU5MiAxMy4wOTA4MTA4MzY5MjQxOTMgQTM4IDM4IDAgMCAxIC0zNy40Mzk5NTE5MjMwNDYwNSA2LjUwMDAwMDAwMDAwMDAwMzYgTC0zNy40Mzk5NTE5MjMwNDYwNSA2LjUwMDAwMDAwMDAwMDAwMzYgTC00Ny40Mzk5NTE5MjMwNDYwNSA2LjUwMDAwMDAwMDAwMDAwNCBMLTQ3LjQzOTk1MTkyMzA0NjA2IC02LjQ5OTk5OTk5OTk5OTk5MyBMLTM3LjQzOTk1MTkyMzA0NjA2IC02LjQ5OTk5OTk5OTk5OTk5NCBBMzggMzggMCAwIDEgLTM1LjY3Mzk0OTQ4MTgyNTkzIC0xMy4wOTA4MTA4MzY5MjQxNjcgTC0zNS42NzM5NDk0ODE4MjU5MyAtMTMuMDkwODEwODM2OTI0MTY3IEwtNDQuMzM0MjAzNTE5NjcwMzIgLTE4LjA5MDgxMDgzNjkyNDE2MyBMLTM3LjgzNDIwMzUxOTY3MDMyNSAtMjkuMzQ5MTQxMDg2MTIxODcgTC0yOS4xNzM5NDk0ODE4MjU5MzYgLTI0LjM0OTE0MTA4NjEyMTg3IEEzOCAzOCAwIDAgMSAtMjQuMzQ5MTQxMDg2MTIxODkzIC0yOS4xNzM5NDk0ODE4MjU5MiBMLTI0LjM0OTE0MTA4NjEyMTg5MyAtMjkuMTczOTQ5NDgxODI1OTIgTC0yOS4zNDkxNDEwODYxMjE4OTcgLTM3LjgzNDIwMzUxOTY3MDMwNCBMLTE4LjA5MDgxMDgzNjkyNDIgLTQ0LjMzNDIwMzUxOTY3MDMwNCBMLTEzLjA5MDgxMDgzNjkyNDE5NSAtMzUuNjczOTQ5NDgxODI1OTIgQTM4IDM4IDAgMCAxIC02LjUwMDAwMDAwMDAwMDAwNSAtMzcuNDM5OTUxOTIzMDQ2MDUgTC02LjUwMDAwMDAwMDAwMDAwNSAtMzcuNDM5OTUxOTIzMDQ2MDUgTC02LjUwMDAwMDAwMDAwMDAwNyAtNDcuNDM5OTUxOTIzMDQ2MDUgTDYuNDk5OTk5OTk5OTk5OTkgLTQ3LjQzOTk1MTkyMzA0NjA2IEw2LjQ5OTk5OTk5OTk5OTk5MiAtMzcuNDM5OTUxOTIzMDQ2MDYgQTM4IDM4IDAgMCAxIDEzLjA5MDgxMDgzNjkyNDE0OSAtMzUuNjczOTQ5NDgxODI1OTQgTDEzLjA5MDgxMDgzNjkyNDE0OSAtMzUuNjczOTQ5NDgxODI1OTQgTDE4LjA5MDgxMDgzNjkyNDE0MiAtNDQuMzM0MjAzNTE5NjcwMzMgTDI5LjM0OTE0MTA4NjEyMTg0NyAtMzcuODM0MjAzNTE5NjcwMzQgTDI0LjM0OTE0MTA4NjEyMTg1NCAtMjkuMTczOTQ5NDgxODI1OTUgQTM4IDM4IDAgMCAxIDI5LjE3Mzk0OTQ4MTgyNTkyIC0yNC4zNDkxNDEwODYxMjE4OTMgTDI5LjE3Mzk0OTQ4MTgyNTkyIC0yNC4zNDkxNDEwODYxMjE4OTMgTDM3LjgzNDIwMzUxOTY3MDMwNCAtMjkuMzQ5MTQxMDg2MTIxODk3IEw0NC4zMzQyMDM1MTk2NzAzMDQgLTE4LjA5MDgxMDgzNjkyNDIgTDM1LjY3Mzk0OTQ4MTgyNTkyIC0xMy4wOTA4MTA4MzY5MjQxOTcgQTM4IDM4IDAgMCAxIDM3LjQzOTk1MTkyMzA0NjA1IC02LjUwMDAwMDAwMDAwMDAwNyBNMCAtMjBBMjAgMjAgMCAxIDAgMCAyMCBBMjAgMjAgMCAxIDAgMCAtMjAiIGZpbGw9IiNlMTViNjQiPjwvcGF0aD48L2c+PC9nPjwvc3ZnPg=='

function retrieveStreamerData (SpyID) {
    retrievers = {
        twitch: require('./twitch.js').getUserStatus
    }

    let userInfo = SpyID.split(':')
    let provider = retrievers[userInfo[0]]
    let name = retrievers[userInfo[1]]
    userInfo.shift()
    userInfo.shift()

    let escapedID = SpyID.replace(/:/g, "-")

    let temptab = renderTab(name, gearspingif, escapedID)
    let tempctx = document.querySelector(`#${escapedID}-ctx-ov`)

    provider(userInfo.join(':'), data => {
        if (!data) {
            alert('api request error for ' + userInfo.join(':'))
            return
        }

        tempctx.parentNode.removeChild(tempctx)
        temptab.parentNode.removeChild(temptab)

        renderTab(data.display_name, data.logo, escapedID, true)

        Taiga.render({
            'section#window_content': [
                {
                    tag: `div.tab.profile#${escapedID}`,
                    children: [
                        {
                            tag: 'div.video-container',
                            children: [
                                {
                                    tag: `video#watch${escapedID}`,
                                },
                                {
                                    tag: "div.controls",
                                    children: [
                                        {
                                            tag: 'button.hasIcon',
                                            attributes: {
                                                style: `background: url(${require('./img/play.svg')}) no-repeat center`
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            tag: 'div.info',
                            children: [
                                {
                                    tag: 'div.title',
                                    children: [
                                        {
                                            tag: 'h1',
                                            data: data.stream_name
                                        },
                                        {
                                            tag: 'p',
                                            data: data.game
                                        }
                                    ]
                                },
                                {
                                    tag: 'div.a-center',
                                    children: [
                                        {
                                            tag: 'img',
                                            attributes: {
                                                src: require('./img/heart.svg')
                                            }
                                        },
                                        {
                                            tag: 'span.followers',
                                            data: data.followers
                                        },
                                        {
                                            tag: 'img',
                                            attributes: {
                                                src: require('./img/views.svg')
                                            }
                                        },
                                        {
                                            tag: 'span.views',
                                            data: data.views
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        })
    })
}

storage.subscribed.forEach(retrieveStreamerData)