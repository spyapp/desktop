const Taiga = require('./taiga')
const { remote } = require('electron')
const twitch = require('./twitch.js')
const renderTab = require('./tabRender.js')
const changeActiveTab = require('./tabs.js')

let intervalId = -1
function randLoadingEmoji() {
    let emojis = [
        '🔥',
        '🐵',
        '🐶',
        '🐺',
        '☕',
        'not 💯',
        '🛀',
        '⚙️',
        '🙄'
    ]

    let emojiID = Math.round(Math.random() * emojis.length) % emojis.length
    return emojis[emojiID]
}

function updateLoadingEmoji() {
    document.querySelector('.loading-dot').innerText = randLoadingEmoji()
}

function renderSubscriptionDialog () {
    Taiga.render({
        body: [
            {
                tag: 'div#subscribe-dialog-overlay.window-overlay',
                on: {
                    click: () => {
                        clearInterval(intervalId)
                        Taiga.render({
                            'div#subscribe-dialog-overlay.window-overlay': false
                        })
                    }
                },
                children: [
                    {
                        tag: 'div.window-dialog#subscribe-dialog',
                        on: {
                            click: (_, event) => {
                                event.stopPropagation()
                            }
                        },
                        children: [
                            {
                                tag: 'div.loading-dot',
                                data: randLoadingEmoji()
                            },
                            {
                                tag: 'h1',
                                data: 'Zasubskrybuj broadcastera'
                            },
                            {
                                tag: 'p',
                                data: 'Wprowadź link do kanału lub natychmiastowe zaproszenie. Zaproszenie będzie wyglądać jak któreś z tych:'
                            },
                            {
                                tag: 'small',
                                data: 'spy://twitch:48401436'
                            },
                            {
                                tag: 'small',
                                data: 'https://twitch.tv/mloteczka'
                            },
                            {
                                tag: 'small',
                                data: 'https://spyapp.cos.ovh/i/IfNe3a'
                            },
                            {
                                tag: 'input#subscribe-input',
                                attributes: {
                                    type: 'text'
                                }
                            },
                            {
                                tag: 'span#subscribe-error',
                                data: 'Kanał lub zaproszenie'
                            },
                            {
                                tag: 'footer.dialog-footer',
                                children: [
                                    {
                                        tag: 'button',
                                        data: 'Subskrybuj',
                                        on: {
                                            click: () => {
                                                let input = document.querySelector('#subscribe-input')
                                                let url = input.value
                                                url = url.replace(/^(http(s|):\/\/)/, "")

                                                let dialog = document.querySelector('#subscribe-dialog')
                                                dialog.className = 'window-dialog locked'
                                                intervalId = setInterval(updateLoadingEmoji, 3000)

                                                let errorMessage = document.querySelector('#subscribe-error')

                                                if (url.startsWith('twitch.tv')) {
                                                    twitch.getUserId(url.substr(10), (res) => {
                                                        if (!res) {
                                                            alert('api request fail: user resolve')
                                                        }

                                                        let escapedID = `u-${res.display_name}-${res._id}`

                                                        global.storage.subscribed.push(`twitch:${res.display_name}:${res._id}`)
                                                        global.writeStorage()

                                                        twitch.getUserStatus(res._id, (data) => {
                                                            if (!data) {
                                                                alert('api request fail: user resolve')
                                                            }


                                                            renderTab(data.display_name, data.logo, escapedID, true)

                                                            Taiga.render({
                                                                'section#window_content': [
                                                                    {
                                                                        tag: `div.tab#${escapedID}`,
                                                                        children: [
                                                                            {
                                                                                tag: 'section.profile_banner',
                                                                                attributes: {
                                                                                    style: `background: url(${data.banner}) center no-repeat #151515;background-size: cover;`
                                                                                }
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                            })

                                                            Taiga.render({
                                                                '#subscribe-dialog-overlay': false
                                                            })

                                                            clearInterval(intervalId)
                                                        })
                                                    })
                                                } else if (url.startsWith('smashcast.tv')) {
                                                    errorMessage.innerText = 'Pewnego dnia zawita tu Smashcast. Kiedyś.'
                                                    dialog.className = 'window-dialog'
                                                    clearInterval(intervalId)
                                                } else if (url.startsWith('spyapp.cos.ovh')) {
                                                    errorMessage.innerText = 'Jeszcze nie.'
                                                    dialog.className = 'window-dialog'
                                                    clearInterval(intervalId)
                                                } else if (url.startsWith('spy://')) {
                                                    errorMessage.innerText = 'Mhm.'
                                                    dialog.className = 'window-dialog'
                                                    clearInterval(intervalId)
                                                } else {
                                                    errorMessage.innerText = 'Błędne zaproszenie.'
                                                    dialog.className = 'window-dialog'
                                                    clearInterval(intervalId)
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    })
}

Taiga.render({
    head: [
        {
            tag: 'title',
            data: 'Spy'
        }
    ],
    body: [
        {
            tag: 'div.window_container',
            children: [
                {
                    tag: 'section#window_tabs',
                    children: [
                        {
                            tag: 'div.icon.less_circle',
                            attributes: {
                                tabId: 'tab__overview',
                                style: `background: url(${require('./img/icon.svg')}) #353535`
                            },
                            on: {
								click: changeActiveTab
							}
                        },
                        {
                            tag: 'label',
                            data: '0 online'
                        },
                        {
                            tag: 'div.separator'
                        },
                        {
                            tag: 'div.dashed_icon#subscribe-button',
							data: '+',
							on: {
								click: renderSubscriptionDialog
							}
                        }
                    ]
                },
                {
                    tag: 'section#window_decoration',
                    children: [
                        {
                            tag: 'div.window_decoration__shelf',
                            data: 'Spy'
                        },
                        {
                            tag: 'div.window_decoration__controls',
                            children: [
                                {
                                    tag: 'button.window_decoration__controls__minimize',
									on: {
										click: () => {
											let window = remote.getCurrentWindow()
											window.minimize()
										}
									}
                                },
                                {
                                    tag: 'button.window_decoration__controls__maximize',
									on: {
										click: () => {
											let window = remote.getCurrentWindow()
											window.maximize()
										}
									}
                                },
                                {
                                    tag: 'button.window_decoration__controls__close',
									on: {
										click: () => {
											let window = remote.getCurrentWindow()
											window.close()
										}
									}
                                }
                            ]
                        }
                    ]
                },
                {
                    tag: 'section#window_content',
                    children: [
                        {
                            tag: 'div.tab.activetab',
                            attributes: {
                                tab: 'overview'
                            }
                        }
                    ]
                }
            ]
        }
    ]
})