const remote = require('electron').remote
const electronWindow = remote.getCurrentWindow()

electronWindow.setSize(storage.windowBounds.width, storage.windowBounds.height, true)

window.addEventListener('beforeunload', (e) => {
    console.log('>>>> saving window bounds');

    let {width, height} = electronWindow.getBounds()

    storage.windowBounds = {
        width: width,
        height: height
    }

    writeStorage()

    return false
})