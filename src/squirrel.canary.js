const electronInstaller = require('electron-winstaller')

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: `${__dirname}/../out/win32/SpyCanary-win32-${process.argv[2]}`,
    authors: 'rylat',
    outputDirectory: `${__dirname}/../out/win32/Installer-${process.argv[2]}`,
    exe: 'SpyCanary.exe',
    setupIcon: `${__dirname}/img/canaryicon.ico`,
    iconUrl: `${__dirname}/img/canaryicon.ico`,
//    remoteReleases: `https://repo.cos.ovh/spy/win${process.argv[2]}`
})
console.log(`${process.argv[2]} out: ${__dirname}/../out/win32/SpyCanary-win32-${process.argv[2]}`)

resultPromise.then(() => console.log("It worked!"), (e) => {console.log(`No dice: ${e.message}`); process.exit(1)})