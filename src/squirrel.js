var electronInstaller = require('electron-winstaller');

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: `${__dirname}/../out/Spy-win32-${process.argv[2]}`,
    authors: 'rylat',
    outputDirectory: `${__dirname}/../out/installer-win32-${process.argv[2]}`,
    exe: 'Spy.exe',
    setupIcon: `${__dirname}/img/appicon.ico`,
    iconUrl: `${__dirname}/img/appicon.ico`,
    loadingGif: `${__dirname}/img/install.gif`
});

resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));