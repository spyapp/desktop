const electron = require('electron')
const path = require('path')
const app = electron.app

module.exports.handleSquirrelEvents = function () {
    if (process.argv.length === 1) {
        return false;
    }

    const ChildProcess = require('child_process')

    const appFolder = path.resolve(process.execPath, '..')
    const rootAtomFolder = path.resolve(appFolder, '..')
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'))
    const exeName = path.basename(process.execPath)

    const spawn = function(command, args) {
        let spawnedProcess, error

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {detached: true})
        } catch (error) {}

        return spawnedProcess
    }

    const spawnUpdate = function(args) {
        return spawn(updateDotExe, args)
    }

    const squirrelEvent = process.argv[1]
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            spawnUpdate(['--createShortcut', exeName])

            app.setAsDefaultProtocolClient('sabat-czarownic')

            setTimeout(app.quit, 1000)
            return true

        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers

            // Remove desktop and start menu shortcuts
            spawnUpdate(['--removeShortcut', exeName])

            app.removeAsDefaultProtocolClient('sabat-czarownic')

            setTimeout(app.quit, 1000)
            return true

        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated

            app.quit()
            return true
    }
}