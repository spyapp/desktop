const fs = require('fs')
const {app} = require('electron').remote
let userData = app.getPath('userData')
let storage = null

function writeStorage() {
    fs.writeFile(`${userData}/storage.json`, JSON.stringify(global.storage), (error) => {
        if (error) {
            alert("storage write error; this dialog may pop up few times again. data will be not persistent.")
        }
    })
}

try {
    storage = JSON.parse(fs.readFileSync(`${userData}/storage.json`))
} catch (_) {
    storage = {
        subscribed: ['twitch:Teo:48401436'],
        useWebsockets: 0,
        windowBounds: {
            width: 1280,
            height: 768
        }
    }

    global.storage = storage

    writeStorage()
}

global.storage = storage
global.writeStorage = writeStorage