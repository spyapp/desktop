const Taiga = require('./taiga')
const changeActiveTab = require('./tabs.js')

function getContextMenuHandler(DisplayName, OriginID, isRemovable) {
	let ctxmenuid = `${OriginID}-ctx`

	Taiga.render({
		body: [
			{
				tag: `div.window-overlay#${ctxmenuid}-ov.translucent`,
				attributes: {
					style: 'display:none'
				},
				on: {
					click: (view) => {
						view.style.display = 'none'
					}
				},
				children: [
                    {
                        tag: `ul.context-menu#${ctxmenuid}`
                    }
				]
			}

		]
	})

	if (isRemovable) {
		Taiga.renderNode({
            tag: 'li',
            data: 'Unsubscribe',
			on: {
            	click: () => {
                    Taiga.render({
                        body: [
                            {
                                tag: 'div#unsubscribe-dialog-overlay.window-overlay',
                                on: {
                                    click: () => {
                                        Taiga.render({
                                            'div#unsubscribe-dialog-overlay.window-overlay': false
                                        })
                                    }
                                },
                                children: [
                                    {
                                        tag: 'div.window-dialog#unsubscribe-dialog',
                                        on: {
                                            click: (_, event) => {
                                                event.stopPropagation()
                                            }
                                        },
                                        children: [
                                            {
                                                tag: 'h1',
                                                data: 'Na pewno?'
                                            },
											{
												tag: 'p',
												data: `Usuniesz za chwilę ${DisplayName} ze swojej listy subskrybowanych. Tego działania nie można cofnąć.`
											},
                                            {
                                                tag: 'footer.dialog-footer',
                                                children: [
                                                    {
                                                        tag: 'button',
                                                        data: 'Potwierdzam',
                                                        on: {
                                                        	click: () => {
                                                        		let tabid = ctxmenuid.substr(0, ctxmenuid.length - 4)
																let userid = tabid.replace(/-/g, ':')
                                                                global.storage.subscribed.splice(global.storage.subscribed.indexOf(userid), 1);
																global.writeStorage()

																let renderFalseObj = {
                                                                    '#unsubscribe-dialog-overlay': false
																}

                                                                renderFalseObj[`#${tabid}`] = false
                                                                renderFalseObj[`#u-${tabid}`] = false

																Taiga.render(renderFalseObj)
															}
														}
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    })
				}
			}
        }, document.querySelector(`#${ctxmenuid}`))
	}

	if (global.isCanary) {
        Taiga.renderNode({
            tag: 'li',
            data: 'DEBUG: CTX ID',
			on: {
            	click: () => {
            		alert(`CONTEXT MENU VIEW ID: ${ctxmenuid}`)
				}
			}
        }, document.querySelector(`#${ctxmenuid}`))
	}

    let ctx = document.querySelector(`#${ctxmenuid}`)
    let overlay = document.querySelector(`#${ctxmenuid}-ov`)

	return (view, event) => {
		event.preventDefault()

        overlay.style.display = 'block'
        ctx.style.top = `${event.pageY}px`
        ctx.style.left = `${event.pageX}px`
	}
}

module.exports = function renderTab (name, iconUrl, boundId, removable = false) {
	Taiga.render({
		'#window_tabs': [
			{
				tag: `div.icon#u-${boundId}`,
				attributes: {
					style: `background: ${iconUrl !== null ? `url(${iconUrl})` : ''} center #353535; background-size: cover`,
					tab: `${boundId}`
				},
				on: {
					click: changeActiveTab,
                    contextmenu: getContextMenuHandler(name, boundId, removable)
				},
				children: iconUrl === null ? [
					{
						tag: 'span',
						data: name[0]
					}
				] : []
			}
		]
	})

	if (iconUrl === null) {
	}

	let subscribebtn = document.getElementById('subscribe-button')
	let tabscontainer = subscribebtn.parentNode
	tabscontainer.removeChild(subscribebtn)
	tabscontainer.appendChild(subscribebtn)

	return document.querySelector(`#u-${boundId}`)
}