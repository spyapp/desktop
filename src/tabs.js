module.exports = function (view) {
	let newActiveTab = document.querySelector(`#${view.getAttribute('tab')}`)
    let oldActiveTab = document.querySelector('.activetab')
    newActiveTab.className += ' activetab'
    oldActiveTab.className = oldActiveTab.className.replace('activetab', '')
}