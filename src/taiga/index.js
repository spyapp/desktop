module.exports = {
    renderNode: function (node, parent) {
        let selector = node.tag.split(/(?=\.)|(?=#)|(?=\[)/)

        let docnode = document.createElement(selector[0])

        for (let event in node.on) {
            docnode.addEventListener(event, node.on[event].bind(docnode, docnode))
        }

        for (let i = 1; i < selector.length; i++) {
            switch (selector[i][0]) {
                case '#':
                    if (docnode.id.length !== 0) {
                        docnode.id += ' '
                    }

                    docnode.id += selector[i].substr(1)
                    break
                case '.':
                    if (docnode.className.length !== 0) {
                        docnode.className += ' '
                    }

                    docnode.className += selector[i].substr(1)
                    break
            }
        }

        if (node.data) {
            if (node.html) {
                docnode.innerHTML = node.data
            } else {
                docnode.innerText = node.data
            }
        }

        if (node.attributes) {
            for (let attribute in node.attributes) {
                docnode.setAttribute(attribute, node.attributes[attribute])
            }
        }

        if (node.datasource) {
            if (!node.children) {
                node.children = []
            }

            for (let row in node.datasource) {
                node.children.push(node.onnewitem(row, node.datasource[row]))
            }
        }

        if (node.save) {
            this.saved_object = docnode
        }

        parent.appendChild(docnode)

        if (node.children) {
            node.children.forEach(childnode => {
                this.renderNode(childnode, docnode)
            })
        }
    },
    render: function (structure) {
        for (let element in structure) {
            let docnode = document.querySelector(element)

            if (!structure[element]) {
                docnode.parentNode.removeChild(docnode)
                continue
            }

            structure[element].forEach(node => {
                this.renderNode(node, docnode)
            })
        }
    }
}