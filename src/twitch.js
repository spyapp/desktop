const APPLICATION_ID = 'bvule5jc75mir2wg26rtw1ws6vm45q'
const https = require('https')

module.exports.getUserId = function (User, Callback) {
	let options = {
		host: 'api.twitch.tv',
		port: 443,
		path: `/kraken/users?login=${User}`,
		method: 'GET',
		headers: {
			accept: 'application/vnd.twitchtv.v5+json',
			'client-id': 'uo6dggojyb8d6soh92zknwmi5ej1q2'
		}
	}
	
	https.request(options, res => {
        let response = ""

		res.on('data', data => {
			response += data
		})

        res.on('end', () => {
            if (res.statusCode !== 200) {
                Callback(false)
            }

            let json = JSON.parse(response)

            Callback(json['users'][0])
        })
	}).end()
}

module.exports.getUserStatus = function (ID, Callback) {
    let options = {
        host: 'api.twitch.tv',
        port: 443,
        path: `/kraken/channels/${ID}`,
        method: 'GET',
        headers: {
            accept: 'application/vnd.twitchtv.v5+json',
            'client-id': 'uo6dggojyb8d6soh92zknwmi5ej1q2'
        }
    }

    https.request(options, res => {
        let response = ""

        res.on('data', data => {
            response += data
        })

        res.on('end', () => {
            if (res.statusCode !== 200) {
                Callback(false)
            }

            let json = JSON.parse(response)
            console.log(json)

            Callback({
				stream_name: json.status,
				display_name: json.display_name,
				logo: json.logo,
				game: json.game,
                followers: json.followers,
                views: json.views
			})
        })
    }).end()
}