const Taiga = require('./taiga')

if (storage.useWebsockets === 0) {
    Taiga.render({
        body: [
            {
                tag: 'div#websockets-dialog-overlay.window-overlay',
                on: {
                    click: () => {
                        Taiga.render({
                            'div#websockets-dialog-overlay.window-overlay': false
                        })

                        storage.useWebsockets = 1
                        writeStorage()
                    }
                },
                children: [
                    {
                        tag: 'div.window-dialog#websockets-dialog',
                        on: {
                            click: (_, event) => {
                                event.stopPropagation()
                            }
                        },
                        children: [
                            {
                                tag: 'h1',
                                data: 'Zezwól na trwałe połączenia'
                            },
                            {
                                tag: 'p',
                                data: 'Możesz otrzymywać powiadomienia, o streamach swoich ulubieńców, z większą dokładnością.'
                            },
                            {
                                tag: 'p',
                                data: 'Zezwolenie na trwałe połączenia zastąpi kilka zapytań, wysyłanych co jakiś czas, pojedynczym połączeniem do serwera, który będzie informować aplikację o nowym statusie streamera.'
                            },
                            {
                                tag: 'small',
                                data: 'W związku z tym na serwer wysłane zostaną Twoje aktualne subskrypcje i unikalny identyfikator.'
                            },
                            {
                                tag: 'small',
                                data: 'Dane wysyłane na serwer będą szyfrowane i nigdy nie zostaną udostępnione.',
                                attributes: {
                                    style: 'margin-bottom: 1em'
                                }
                            },
                            {
                                tag: 'footer.dialog-footer',
                                children: [
                                    {
                                        tag: 'button.grey',
                                        data: 'Nie zezwalaj',
                                        on: {
                                            click: () => {
                                                global.storage.useWebsockets = 1

                                                Taiga.render({
                                                    'div#websockets-dialog-overlay.window-overlay': false
                                                })

                                                writeStorage()
                                            }
                                        }
                                    },
                                    {
                                        tag: 'button',
                                        data: 'Zezwól',
                                        on: {
                                            click: () => {
                                                global.storage.useWebsockets = 2

                                                Taiga.render({
                                                    'div#websockets-dialog-overlay.window-overlay': false
                                                })

                                                writeStorage()
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    })
}