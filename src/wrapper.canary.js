const electron = require('electron')
const app = electron.app
const fs = require('fs')
const path = require('path')
const url = require('url')
const {ipcMain} = require('electron')
const {handleSquirrelEvents} = require('./squirrelevents.js')

app.setAppUserModelId('com.squirrel.spy-canary.SpyCanary')

if (handleSquirrelEvents()) {
    app.exit(0)
    process.exit(0)
}

const BrowserWindow = electron.BrowserWindow

let mainWindow

function createWindow () {
    mainWindow = new BrowserWindow({
        width: 1280,
        height: 768,
        icon: path.join(__dirname, 'img/canaryicon.png'),
        frame: false,
        title: 'Spy',
        backgroundColor: '#242424'
    })

    if (fs.existsSync(`${__dirname}/../webpack.canary.config.js`)) {
        mainWindow.loadURL('http://localhost:8080/renderer')
    } else {
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, 'index.html'),
            protocol: 'file:',
            slashes: true
        }))
    }

    mainWindow.on('closed', () => {
        mainWindow = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow()
    }
})

const template = fs.readFileSync(`${__dirname}/notification.html`)
ipcMain.on('notification-request', (event, html) => {
    return
    let notificationwindow

    notificationwindow = new BrowserWindow({
        width: 350,
        height: 92,
        frame: false,
        backgroundColor: '#fff',
        parent: mainWindow
    })

    notificationwindow.setAlwaysOnTop(true)

    notificationwindow.loadURL(url.format({
        pathname: 'text/html;' + html,
        protocol: 'data:',
        slashes: true
    }))

    notificationwindow.on('closed', () => {
        notificationwindow = null
    })
})