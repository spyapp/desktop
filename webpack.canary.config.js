const ExtractTextPlugin = require("extract-text-webpack-plugin")

const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
})

module.exports = {
    entry: {
        renderer: `${__dirname}/src/canaryentry.js`,
        index: `${__dirname}/src/wrapper.canary.js`
    },
    output: {
        path: `${__dirname}/src/build.canary`,
        filename: "[name].js"
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: 'sass-loader',
                options: {
                    url: false,
                    outputStyle: 'compressed'
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader',
                options: { url: false }
            },
            {
                test: /\.(png|jpeg|ttf|woff2|woff|svg)$/,
                loader: 'url-loader',
                options: {
                    limit: 200000
                }
            }
        ],
        rules: [
            {
                test: /\.(s|)css$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader",
                        options: { url: false }
                    }, {
                        loader: "sass-loader",
                        options: {
                            url: false,
                            outputStyle: 'compressed',
                            sourceMap: false
                        }
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },

            {
                test: /\.(png|jpeg|ttf|woff2|woff|svg)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 200000
                    }
                }
            }
        ]
    },
    externals: {
        'electron': "require('electron')",
        'fs': "require('fs')",
        'path': "require('path')",
        'child_process': "require('child_process')"
    },
    plugins: [
        new ExtractTextPlugin("styles.css")
    ]
};