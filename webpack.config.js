const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ClosureCompilerPlugin = require('webpack-closure-compiler');

const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: `${__dirname}/src/entry.js`,
    output: {
        path: `${__dirname}/src/build`,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: 'sass-loader',
                options: {
                    url: false,
                    outputStyle: 'compressed',
                    sourceMap: false
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader',
                options: { url: false }
            },
            {
                test: /\.(png|jpeg|ttf|woff2|woff)$/,
                use: [
                    { loader: 'url-loader', options: { limit: 100000 } }
                ]
            }
        ],
        rules: [
            {
                test: /\.(s|)css$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader",
                        options: { url: false }
                    }, {
                        loader: "sass-loader",
                        options: {
                            url: false,
                            outputStyle: 'compressed',
                            sourceMap: false
                        }
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            }
        ]
    },
    externals: {
        'electron': "require('electron')",
        'fs': "require('fs')"
    },
    plugins: [
        new ExtractTextPlugin("styles.css"),
        new ClosureCompilerPlugin({
            compiler: {
                language_in: 'ECMASCRIPT6',
                language_out: 'ECMASCRIPT5',
                compilation_level: 'SIMPLE'
            },
            concurrency: 3,
        })
    ]
};